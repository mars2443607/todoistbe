const express = require('express');
const app = express();

app.get('/', (req, res) => {
  console.log(req.url);
  res.redirect('todoistclone://' + req.url);
});

app.listen(8084, () => console.log('Example app listening on port 8084!'));
